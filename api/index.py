from flask import Flask, request, jsonify
from requests import get 
import string
import json
import secrets
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

#password generator
@app.route('/')
@cross_origin()
def index():
    
    password_chars = string.ascii_letters + string.punctuation  + string.digits
    password = "".join(secrets.choice(password_chars) for i in range(12))

    return jsonify(name='PasswordGenerator',data=password)


#for postman
@app.route('/post', methods=["POST"])
def testpost():
     input_json = request.get_json(force=True) 
     dictToReturn = {'text':input_json['text']}
     return jsonify(dictToReturn)


#test GET request
@app.route('/test')
def get_data():
    # return requests.get('http://127.0.0.1:5000/').content

    content = get("https://ovsmar.pythonanywhere.com/").text
    data = json.loads(content)
    return data['data']



